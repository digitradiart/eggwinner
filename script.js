window.addEventListener("DOMContentLoaded", () => {
  const images = Array.from(document.querySelectorAll(".gallery img"));
  images.forEach((img) => {
    const image = new Image();
    image.addEventListener("load", () => {
      img.src = image.src;
    });
    image.src = img.getAttribute("data-src");
  });
});
